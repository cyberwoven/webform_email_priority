<?php

namespace Drupal\webform_email_priority\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandler\EmailWebformHandler;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Emails a webform submission.
 *
 * @WebformHandler(
 *   id = "email",
 *   label = @Translation("Email"),
 *   category = @Translation("Notification"),
 *   description = @Translation("Sends a webform submission via an email."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class WebformEmailPriorityHandler extends EmailWebformHandler {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $defaultConfiguration = parent::defaultConfiguration();
    $defaultConfiguration['priority'] = '';
    return $defaultConfiguration;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultConfigurationValues() {
    $defaultConfigurationValues = parent::getDefaultConfigurationValues();
    $defaultConfigurationValues['priority'] = '';
    return $defaultConfigurationValues;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildConfigurationForm($form, $form_state);

    $this->applyFormStateToConfiguration($form_state);

    // Give weight to "Included email values/markup" section,
    // so we can bump the Priority section up above it.
    $form['elements']['#weight'] = 2;

    // Priority.
    $form['priority_flag'] = [
      '#type' => 'details',
      '#title' => $this->t('Message Priority'),
      '#help' => $this->t('This is the "Priority" email header.  It is rarely needed, but can be used to inform the recipient of the importance of the message.'),
      '#open' => TRUE,
      '#weight' => 1, //
    ];

    $form['priority_flag']['priority'] = [
      '#type' => 'select',
      '#title' => $this->t('Priority level of this email.'),
      '#description' => $this->t('Select the priority level of this email.'),
      '#options' => [
        '5' => 'Low',
        '3' => 'Normal',
        '1' => 'High',
      ],
      '#default_value' => $this->configuration['priority'] ?: '3',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['priority'] = $values['priority_flag']['priority'];
    parent::submitConfigurationForm($form, $form_state);
  }

}
